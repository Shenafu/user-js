// ==UserScript==
// @include http://forecast.weather.gov/*
// @include https://forecast.weather.gov/*
// ==/UserScript==

function degreeToComfort(deg) {
	// convert Fahrenheit to Comfort
	// 68 -> 0
	// 32-> -36
	// 104 -> +36

	var d = deg - 68;
	if (d>0) {
		d = "+" + d;
	}
	return d;
}

function degreeToHSV(deg) {
	var h =  (deg >= 0 ? 112 - deg * 4 : 240 + deg * 2); // 0' is green = 90 hue
	var s = 100;
	var l = 50 - (deg / 1.5);

	var hsl = "hsl(" + h + ", " + s + "%, " + l + "%)";
	return hsl;
}

function degreeToRGB(deg) {
	// 0 is green
	// +deg = red
	// -deg = blue/white
	var r = 128 - (deg * 1 > 0 ? -deg * 4 : 0);
	var g = 224 - (deg * 1 > 0 ? deg * 4 : deg * 1);
	var b = 128 - (deg * 1 < 0 ? deg * 4 : deg * 4);

	var rgb = "rgb(" + r + ", " + g + ", " + b + ")";
	return rgb;
}

function ComfortScriptLoad() {
	var table = document.getElementsByTagName("table")[7]; // 8th table lists hourly temps data
	var rows = [ table.rows[3].getElementsByTagName('td'),
		table.rows[11].getElementsByTagName('td')
	]; // 4th, 11th row lists temps

	for (var j=0, lj=rows.length; j<lj; j++) {
		for (var i=1, li=rows[j].length; i<li; i++) {
			var cell = rows[j][i];
			cell.innerText = degreeToComfort(cell.innerText);
			cell.style.color = degreeToHSV(cell.innerText);
			cell.style.fontWeight = "bolder";
			cell.style.fontSize = "16pt";
			cell.style.padding = "3px";
		}
	}
}

// run script on document load
window.addEventListener('load', ComfortScriptLoad
, false);