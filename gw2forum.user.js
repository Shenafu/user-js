// ==UserScript==
// @name	GW2Forum Redirect Fix
// @include https://forum-en.guildwars2.com/*
// @include https://en-forum.guildwars2.com/*
// ==/UserScript==

/*

remove stupid redirect notice from external links
ex.
https://forum-en.guildwars2.com/external?l=http%3A%2F%2Fen.gw2skills.net%2Feditor%2F%3FvhAQRAsc8encfCFoh9fCmfCEgiFVjCtLSjsrOZn2qlMAGhiD-TBSBABmpE8hTCwVK%2FKU%2Fgoq%2FgnGi%2F3fAgnAQAgDg1j6BgUA5J0C-e

don't want this part:
https://forum-en.guildwars2.com/external?l=

new forums:
https://en-forum.guildwars2.com/home/leaving?target=https%3A%2F%2Fprnt.sc%2Fgopsr7

don't want this part:
https://en-forum.guildwars2.com/home/leaving?target=

*/

(function() {

var GW2LINK_URLMATCH = [
	"https://forum-en.guildwars2.com/external?l=",
	"https://en-forum.guildwars2.com/home/leaving?target=",
];



function removeGW2Redirect(node) {
	var urls = node.getElementsByTagName("a");

	for (var i=0, l=urls.length; i<l; i++) {
		var url = urls[i].href;

		for (var j=0, m=GW2LINK_URLMATCH.length; j<m; j++) {
			if (url.indexOf(GW2LINK_URLMATCH[j]) != -1) {

				var sub = decodeURIComponent( url.substring( (GW2LINK_URLMATCH[j].length)) );

				// replace link with new one. old link has crap like popup warning

				var a = document.createElement("a");
				a.href = a.text = sub;
				a.target = "_blank";

				urls[i].parentElement.replaceChild(a, urls[i]);

				break;
			}
		}
	}
}

window.addEventListener('load', function() {
	removeGW2Redirect(document);
	},
	false);


})();
