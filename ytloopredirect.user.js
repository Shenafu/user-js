// ==UserScript==
// @name        YouTube Loop Button Redirect
// @description Redirect to Loop + max windowed
// @namespace   http://shenafu.com/
// @version     1.1.2
// @author      Amuseum
// @license     GPLv3 or later <http://www.gnu.org/licenses/gpl.html>
// @include     http://youtube.com/*
// @include     https://youtube.com/*
// @include     http://www.youtube.com/*
// @include     https://www.youtube.com/*
// @include     http://youtu.be/*
// @include     https://youtu.be/*
// @include     http://www.shenafu.com/smf/
// ==/UserScript==

// javascript: ytp.playVideo();

/*************************************
Youtube API
https://developers.google.com/youtube/player_parameters

PLOYU5izAqDYiB8O8Es33IeEIhc8jAKdeS ~ Hyori playlist
v8Y3NL1KeNQ ~ UGOGIRL
UDVzsIQmFc4 ~ Mr. Big

single video
http://www.youtube.com/v/UDVzsIQmFc4&loop=1&autoplay=1&playlist=UDVzsIQmFc4

playlist
http://www.youtube.com/embed?listType=playlist&list=PLOYU5izAqDYiB8O8Es33IeEIhc8jAKdeS&loop=1&autoplay=1

no controls
http://www.youtube.com/apiplayer?video_id=UDVzsIQmFc4&loop=1&autoplay=1
http://www.youtube.com/apiplayer?loop=1&autoplay=1&listType=playlist&list=PLOYU5izAqDYiB8O8Es33IeEIhc8jAKdeS

googleapi
playlist
http://youtube.googleapis.com/v/v8Y3NL1KeNQ?loop=1&autoplay=1&listType=playlist&list=PLOYU5izAqDYiB8O8Es33IeEIhc8jAKdeS
single video
http://youtube.googleapis.com/v/v8Y3NL1KeNQ?loop=1&autoplay=1&listType=playlist&list=v8Y3NL1KeNQ

//********************************/


if (document.getElementById('watch7-action-panels')) {
	var inp = document.createElement('input');
	inp.setAttribute("type", "button");
	inp.setAttribute("id", "ytLoopVideo");
	inp.setAttribute("value",  "Loop Video");
	inp.addEventListener("click",
		function(e) { Y$YTLoopPlaylist(e, 1); }
	, false);

	var d = document.createElement("div");
	d.appendChild(inp);
	var ws = document.getElementById('watch7-secondary-actions');
	ws.appendChild(d);

	inp = document.createElement('input');
	inp.setAttribute("type", "button");
	inp.setAttribute("id", "ytLoopPlaylist");
	inp.setAttribute("value",  "Loop Playlist");
	inp.addEventListener("click",
		function(e) { Y$YTLoopPlaylist(e, 0); }
	, false);

	d = document.createElement("div");
	d.appendChild(inp);6
	ws.appendChild(d);
}

function getVideoId( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}

function getPlaylistId()
{
  var regexS = "list=(PL[^&]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}

function Y$YTLoopPlaylist(e, videoOnly) {
	var pl = getPlaylistId();
	var v = getVideoId('v');

//*

	//http://youtube.googleapis.com/v/v8Y3NL1KeNQ?loop=1&autoplay=1&listType=playlist&list=v8Y3NL1KeNQ

	if (pl != "" && !videoOnly) {
		// loop entire playlist
		var url = "http://youtube.googleapis.com/v/" + v + "?loop=1&autoplay=1&listType=playlist&list=" + pl;
	}
	else {
		// loop single video
		var url = "http://youtube.googleapis.com/v/" + v + "?loop=1&autoplay=1&playlist=" + v;
	}

	//alert(url);
	window.location = url;
//*/
}
