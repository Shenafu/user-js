// ==UserScript==
// @name	Text2Link
// @namespace	http://shenafu.com/
// @description	Turn text URLs to real links

// @version	1.00
// @author	Lord of Atlantis
// @download	http://www.shenafu.com/opera/text2link.user.js

// ==/UserScript==

// CONSTANTS
var SKIP_ELEMENTS = ['SCRIPT', 'INPUT', 'TEXTAREA'];
var TEXT2LINK_URLMATCH = /(?:(?:https?:\/\/)|(?:www))\S*[^\s'"()!?.,\][]/gi;

function text2link_processtext(textnode, separator) {
	// hold nonURL text
	var matches = textnode.nodeValue.split(separator);
	if(matches.length > 1) {
		var urls = textnode.nodeValue.match(separator);
		var index = 0, p = textnode.parentNode;
		var n = document.createDocumentFragment();
		n.appendChild( document.createTextNode(matches[index]));
		while(index < urls.length) {
			var link = document.createElement('a');
			link.href = urls[index];
			link.appendChild(document.createTextNode(urls[index]));
			n.appendChild(link, textnode);
			index++;
			n.appendChild(document.createTextNode(matches[index]), textnode);
		}
		p.replaceChild(n, textnode);
	}
}

function nestedReplace(node) {
	if(node.tagName && node.tagName == 'A') {
		return;
	}
	if(node.tagName && SKIP_ELEMENTS.indexOf(node.tagName)>-1) {
		return;
	}
	if(node.childNodes.length) {
		for(var i=0; i<node.childNodes.length; i++) {
			nestedReplace(node.childNodes[i]);
		}
		return;
	}
	if(node.nodeName == '#text') {
		text2link_processtext(node, TEXT2LINK_URLMATCH);
		return;
	}
}

function removeAnnoyingTags(node){
	var tagmatch = new Array();
//alert(tagmatch.length);
	tagmatch[0] = "WBR";
	if(node.childNodes) {
		for(var i=0; i<node.childNodes.length; i++) {
			removeAnnoyingTags(node.childNodes[i]);
			for(var j=0;j<tagmatch.length; j++) {
				if(node.childNodes[i].tagName==tagmatch[j]){
					node.removeChild(node.childNodes[i]);
//alert(node.childNodes[i].tagName);
				}
			}
		}
		//alert(node.innerHTML);
	}
}

//nestedReplace(document);
window.addEventListener('load', function() {
	removeAnnoyingTags(document);
	nestedReplace(document.body);
	imagepopup();
	videopopup();
	},
	false);

// INTERFACING EVENTS /////////////////////////////////////////////

function text2link_menucommand() {
	// helper for browser menu commands
	// eg select an unlinked card name, right click to open menu, choose "Text to link" to create a link for all unlinked text that match the selected strng
	if (window.getSelection) {
		var userSelection = window.getSelection();
		var userRange= userSelection.getRangeAt(0);
		var node = userRange.startContainer;
		// some elements like headers dont default to text type
		if (!node.nodeName || node.nodeName != "#text") {
			node = node.childNodes[0];
		}

		if (node.nodeName == "#text") {
			text2link_processtext(node, new RegExp(userSelection, "g"));
		}
	}
}