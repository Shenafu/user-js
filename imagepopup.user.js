// ==UserScript==
// @name	Image_popup
// @namespace	http://shenafu.com/
// @description	Image pops up when mouseover link

// @version	1.10
// @author	Lord of Atlantis
// @download	http://www.shenafu.com/opera/imagepopup.user.js

// ==/UserScript==

// USER PREFERENCES
var TEXTTOIMAGE_ACTIVE = false; // true to turn URLs into images
var IMAGEPOPUP_ONCTRL = true; // popup only when CTRL is held down

// CONSTANTS
var IMAGEPOPUP_IMG_MAX_WIDTH = 600;
var IMAGEPOPUP_IMG_MAX_HEIGHT = 450;
var IMAGEPOPUP_IMG_MIN_WIDTH = 600;
var IMAGEPOPUP_IMG_MIN_HEIGHT = 450;

// FUNCTIONS
function imagepopup_createimg() {
	var img = document.createElement("img");
	img.id = "imagepopupimg";
	img.style.visibility = "hidden";

	img.style.maxWidth = IMAGEPOPUP_IMG_MAX_WIDTH;
	img.style.maxHeight = IMAGEPOPUP_IMG_MAX_HEIGHT;
	img.style.minWidth = IMAGEPOPUP_IMG_MIN_WIDTH;
	img.style.minHeight = IMAGEPOPUP_IMG_MIN_HEIGHT;

	img.style.position = "fixed";
	img.style.top = 0;
	img.style.right = 0;
	img.style.zIndex = "10000";

	img.setAttribute("src", "");

	img.addEventListener("mouseout", function() { imagepopup_mouseout(img); }, false);
	img = document.body.appendChild(img);

	return img;
}

function imagepopup_createimgtemp() {
	img_temp = document.createElement('img');
	img_temp.id = "img_temp";
	img_temp.style.visibility = "hidden";
	img_temp.width = 1;
	img_temp.height = 1;
	img_temp = document.body.appendChild(img_temp);
}

function imagepopup_processlink(link) {
	// only if no img as child
	if ( imagepopup_match(link) && link.getElementsByTagName('img').length < 1 ) {
		if (TEXTTOIMAGE_ACTIVE) {
			var imgel = document.createElement('img');
			if (link.getAttribute("href").match(/svg/)) {
				imgel = document.createElement('object');
				imgel.setAttribute("type", "image/svg+xml");
				imgel.setAttribute("data", link.getAttribute("href"));
			}
			else {
				imgel.setAttribute("src", link.getAttribute("href"));
			}
			var p = link.parentNode;
			p.insertBefore(imgel, link);
			link.style.display = "none";
		}
		else {
			link.addEventListener("mouseover", function() {
				if (!IMAGEPOPUP_ONCTRL || event.ctrlKey) {
					imagepopup_mouseover(this.getAttribute("href"));
				}
			}, false);
			link.addEventListener("mouseout", function() { imagepopup_mouseout(this); }, false);
			img_temp.src = link.getAttribute("href");
		}
	}
}

function imagepopup_mouseover(source) {
	var img = document.getElementById("imagepopupimg");
	img.setAttribute("src", source);
	img.style.visibility = "visible";
}

function imagepopup_mouseout(o) {
	var img = document.getElementById("imagepopupimg");

	var y = event.clientY;
	var x = event.clientX;
	var w = img.offsetWidth;
	var h = img.offsetHeight;
	if ( y <= h && x >= w ) {
		img.style.visibility = "visible";
	}
	else {
		img.style.visibility = "hidden";
	}
}

function imagepopup_match(link) {
	if ( link.href.match(/.+\.(jpe?g|png|bmp|gif|svg)/) ) {
		return true;
	}
	return false;
}

// main function
function imagepopup() {
	// create img
	var img = imagepopup_createimg();
	imagepopup_createimgtemp();

	// go through all links in the page
	var urls = document.getElementsByTagName("a");
	for (i=0; i<urls.length; i++) {
		imagepopup_processlink(urls[i]);
	}
}

// if new anchors are added later, parse those links
// e.g. card search engines using AJAX
window.addEventListener('DOMNodeInserted', function(event) {
	if (event.target.tagName && event.target.tagName.toUpperCase()=="A") {
		imagepopup_processlink(event.target);
	}
}, false);

// run script on document load
// this is called from text2link.js instead
// window.addEventListener("load", function() { imagepopup() }, false);
//imagepopup();
