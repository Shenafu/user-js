/* AutoScroller
* by Yellowfour
* userscript for Opera browsers
* inspired by ASSTR autoscroller
*/

var Y4_Auto$croll = {};

Y4_Auto$croll.dy = 1;

// list sites you want to add double click to start or stop scrolling
// enclose site inside quotes and separate each site with comma
Y4_Auto$croll.dblClickSites = new Array(
//	"file://localhost/"
);

// localized text
Y4_Auto$croll.textStart = "Start";
Y4_Auto$croll.textStop = "Stop";
Y4_Auto$croll.textHide = "Hide";
Y4_Auto$croll.textSpeed = ["Fastest", "Faster", "Fast", "Medium", "Slow", "Slower", "Slowest"];
Y4_Auto$croll.intSpeed = [5, 10, 25, 50, 100, 250, 500];
Y4_Auto$croll.defaultSpeed = 4;
Y4_Auto$croll.scrollRate = Y4_Auto$croll.intSpeed[Y4_Auto$croll.defaultSpeed];


Y4_Auto$croll.scrollPage = function() {
	//var yOffset = window.pageYOffset + Y4_Auto$croll.dy;
	//var xOffset = window.pageXOffset;

	scrollBy(0, Y4_Auto$croll.dy);
}

/* This function actually creates the scroller buttons. */

Y4_Auto$croll.createScroller = function() {
	// make floating form to hold controls

	var floatform = document.createElement("form");
	floatform.name=floatform.id="Y4_Auto$croll_mainForm";
	if (floatform) {
		floatform.style.position="fixed";
		floatform.style.right="100px";
		floatform.style.bottom="2px";
		floatform.style.display="none";
	}

	var newselect = document.createElement("select");
	newselect.name=newselect.id="Y4_Auto$croll_selectElement";
	newselect.addEventListener('change', function() {Y4_Auto$croll.startScroll(this.options[this.selectedIndex].value);}, false);

	var option = [];
	for (i=1; i<=Y4_Auto$croll.textSpeed.length; i++) {
		option[i]=document.createElement("option");
		option[i].text=Y4_Auto$croll.textSpeed[i-1];
		option[i].value=i-1;
		newselect.add(option[i]);
	}
	option[Y4_Auto$croll.defaultSpeed+1].defaultSelected=option[Y4_Auto$croll.defaultSpeed+1].selected="selected";

	// buttons
	var newinput=new Array(2);
	var n = 1;
	newinput[n]=document.createElement("input");
	newinput[n].type="button";
	newinput[n].name=newinput[n].id="Y4_Auto$croll_startButton";
	newinput[n].value=Y4_Auto$croll.textStart;
	newinput[n].addEventListener('click', Y4_Auto$croll.startOrStopScroll, false);
	n++;
	newinput[n]=document.createElement("input");
	newinput[n].type="button";
	newinput[n].name=newinput[n].id="Y4_Auto$croll_hideButton";
	newinput[n].value=Y4_Auto$croll.textHide;
	newinput[n].addEventListener('click', Y4_Auto$croll.hide, false);

	floatform.appendChild(newselect);
	for(i=1; i<=n; i++){
		floatform.appendChild(newinput[i]);
	}
	document.body.appendChild(floatform);
} // end createScroller()

Y4_Auto$croll.hide = function() {
	document.getElementById("Y4_Auto$croll_mainForm").style.visibility = "hidden";
}

Y4_Auto$croll.stopScroll = function() {
	if (Y4_Auto$croll.scrollInterval) {
		clearInterval( Y4_Auto$croll.scrollInterval );
	}
	Y4_Auto$croll.isScrolling = false;
	var startButton = document.getElementById("Y4_Auto$croll_startButton");
	if (startButton) {
		startButton.value = Y4_Auto$croll.textStart;
	}
}

Y4_Auto$croll.startScroll = function(newScrollRate) {
	clearInterval( Y4_Auto$croll.scrollInterval );
	Y4_Auto$croll.isScrolling = true;
	var startButton = document.getElementById("Y4_Auto$croll_startButton");
	if (startButton) {
		startButton.value = Y4_Auto$croll.textStop;
	}

	if (newScrollRate) {
		Y4_Auto$croll.scrollRate = Y4_Auto$croll.intSpeed[newScrollRate];
	}
	Y4_Auto$croll.scrollPage();
	Y4_Auto$croll.scrollInterval = setInterval( Y4_Auto$croll.scrollPage, Y4_Auto$croll.scrollRate);
}

Y4_Auto$croll.startOrStopScroll = function() {
	if (!Y4_Auto$croll.isScrolling) {
		Y4_Auto$croll.startScroll();
	}
	else {
		Y4_Auto$croll.stopScroll();
	}
}

window.addEventListener('load', function() {
	if ( !window.hasAuto$crollForm) {
		Y4_Auto$croll.createScroller();
		window.hasAuto$crollForm = true;
	}

	// add double click to specific sites
	for (var i=0; i < Y4_Auto$croll.dblClickSites.length; i++) {
		if ( document.URL.match(new RegExp(Y4_Auto$croll.dblClickSites[i], 'i')) ) {
			document.body.addEventListener( "dblclick", Y4_Auto$croll.startOrStopScroll, false);
			break;
		}
	}
}, false);
