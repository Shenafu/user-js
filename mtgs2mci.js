// ==UserScript==
// @name	AutoCard_MTG
// @namespace	http://shenafu.com/
// @description	Alter mtgsalvation card links to magiccards.info

// @version	1.0
// @author	Lord of Atlantis
// @download	http://www.shenafu.com/magic/
// @encoding utf-8

// @include	http://mtgsalvation.com/*
// @include	http://*.mtgsalvation.com/*
// @include	http://mtgsalvation.gamepedia.com/*

// ==/UserScript==

var MTGS2MCI_ORIG_RE = new Array(
	new RegExp("http:\/\/www\.mtgsalvation\.com\/cards.*([0-9]+\-|search=)(.*)")
);

var MTGS2MCI_POST_RE = "http://magiccards.info/query?q=cardname";

function mtgs2mci_fixUrl(cardname, link) {
	var src = MTGS2MCI_POST_RE.replace("cardname", cardname);
	link.href = src;
}

function mtgs2mci_findCard(link) {
	var cardname = "";
	var linktext = link.text;
	var href = link.href;
	var url = document.URL;

	// mtgsalvation curse forums
	if (MTGS2MCI_ORIG_RE[0].test(href)) {
		cardname = linktext.replace(MTGS2MCI_ORIG_RE[0], "$2");
	}

	return cardname;
}

function mtgs2mci_modUrl(link) {
	// find card's name
	var cardname = mtgs2mci_findCard(link);

	// if card is found, then do change URL
	if ( cardname != "" ) {
		mtgs2mci_fixUrl(cardname, link);
	}

}

function mtgs2mci() {

	// look for and store all links in the page
	var urls = document.getElementsByTagName("A");

	// if link matches criteria, change URLs
	for (i=0; i<urls.length;i++) {
		var link = urls[i];
		mtgs2mci_modUrl(link);
	}


}

// run script on document load
window.addEventListener('load',
	function() {
		mtgs2mci();
	}
, false);

