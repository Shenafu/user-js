// ==UserScript==
// @exclude http://mail.google.com
// ==/UserScript==
function myRootSignature() {
 var d=document.documentElement;
 var locHost = location.hostname.replace(/^www\./g,'').replace(/\./g,' ');//replaces dots with space
 locHost = locHost.replace(/[^\w- ]/g,'');//removes IDN characters
 locHost = locHost.replace(/\b(\d)/g,'_$1');//prepends leading numbers with underscore
 d.className = (d.className?(d.className+' '):'') + 'mycss' + ' ' + locHost;
 //alert(document.documentElement.className);
}
var myDoneNums = 0;
var myCheckNode = setInterval('myDoneNums++;if(document.documentElement){clearInterval(myCheckNode);myRootSignature();}else if(myDoneNums>6){clearInterval(myCheckNode);document.addEventListener("load",myRootSignature,false);}',200);
