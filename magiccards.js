// ==UserScript==
// @include https://magiccards.info/search.html
// ==/UserScript==

(function() {

var startScript = function() {
	// View as a Spoiler
	var output = document.getElementsByName("v")[0];
	output.value = "spoiler";

	// Get only English cards
	var language = document.getElementById("language");
	language.checked = true;

};

window.addEventListener('load', startScript, false);

})(); // end UserScript