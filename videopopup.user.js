// ==UserScript==
// @name	Video_popup
// @namespace	http://shenafu.com/
// @description	Video pops up when mouseover link and shift key is down

// @version	1.01
// @author	Lord of Atlantis
// @download	http://www.shenafu.com/opera/videopopup.user.js

// ==/UserScript==

var VIDEOPOPUP_IMG_MAX_WIDTH = 560;
var VIDEOPOPUP_IMG_MAX_HEIGHT = 315;
var VIDEOPOPUP_IMG_MIN_WIDTH = 560;
var VIDEOPOPUP_IMG_MIN_HEIGHT = 315;
//var VIDEO_EMBED_URL = "https://www.youtube.com/v/VIDEONAME&autoplay=1";
var VIDEO_EMBED_URL = "https://www.youtube.com/watch?v=VIDEONAME&autoplay=1";

var VIDEOPOPUP_URL_PATTERN = new Array(
	new RegExp("(http:\/\/)?.*\.youtube\.com\/watch\?.*v=(.{11}).*")
	,new RegExp("(http:\/\/)?youtu\.be\/(.{11}).*")
	,new RegExp("(http:\/\/)?.*\.youtube.com/v/(.{11}).*")
);

function videopopup_createiframe() {
	var iframe = document.createElement("iframe");
	iframe.id = "videopopupiframe";
	iframe.style.visibility = "hidden";

	iframe.width = VIDEOPOPUP_IMG_MAX_WIDTH;
	iframe.height = VIDEOPOPUP_IMG_MAX_HEIGHT;

	iframe.style.maxWidth = VIDEOPOPUP_IMG_MAX_WIDTH;
	iframe.style.maxHeight = VIDEOPOPUP_IMG_MAX_HEIGHT;
	iframe.style.minWidth = VIDEOPOPUP_IMG_MIN_WIDTH;
	iframe.style.minHeight = VIDEOPOPUP_IMG_MIN_HEIGHT;

	iframe.style.position = "fixed";
	iframe.style.top = 0;
	iframe.style.right = 0;
	iframe.style.zIndex = "10000";

	iframe.setAttribute("src", "");

	iframe.addEventListener("mouseout", function(event) {
		if (event.ctrlKey) {
			videopopup_mouseout(this);
		}
	}, false);
	iframe = document.body.appendChild(iframe);

	return iframe;
}

function videopopup_mouseover(source) {
	var iframe = document.getElementById("videopopupiframe");
	iframe.setAttribute("src", source);
	iframe.style.visibility = "visible";
}

function videopopup_mouseout(iframe) {
	// stop video
	iframe.setAttribute("src", "");

	iframe.style.visibility = "visible";
	iframe.style.visibility = "hidden";
}

function videopopup_match(link) {
	var video = "";
	var source = "";
	for (var i=0; i < VIDEOPOPUP_URL_PATTERN.length; i++) {
		if ( link.href.match( VIDEOPOPUP_URL_PATTERN[i] ) ) {
			video = link.href.replace( VIDEOPOPUP_URL_PATTERN[i], "$2");
			source = VIDEO_EMBED_URL.replace( "VIDEONAME", video);
			return source;
		}
	}
	return "";
}

function videopopup() {
	// create iframe
	var iframe = videopopup_createiframe();

	var urls = document.getElementsByTagName("a");
	for (i=0; i<urls.length; i++) {
		var link = urls[i];
		var videosrc = videopopup_match(link);
		if ( videosrc != "") {
			link.setAttribute("videosrc", videosrc);
			link.addEventListener("mouseover", function(event) {
				if (event.ctrlKey) {
					videopopup_mouseover(this.getAttribute("videosrc"));
				}
			}, false);
			//link.addEventListener("mouseout", function() { videopopup_mouseout(this); }, false);
		}
	}
}

// this is called from text2link.js instead
// window.addEventListener("load", function() { videopopup() }, false);
//videopopup();
